<?php

use Faker\Generator as Faker;
use App\Link;

$factory->define(App\openGraph::class, function (Faker $faker) {
    return [
        'link_id'     => factory(Link::class)->create()->id,
        'title'       => 'Bitfumes',
        'description' => 'My description',
        'image_url'   => 'https://bitfumes.com/image/logo.png',
    ];
});
