<?php

use Faker\Generator as Faker;

$factory->define(App\Link::class, function (Faker $faker) {
    return [
        'original_url' => 'https://bitfumes.com',
        'short_code'   => $faker->randomNumber(6),
    ];
});
