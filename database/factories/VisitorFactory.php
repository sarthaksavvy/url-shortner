<?php

use Faker\Generator as Faker;
use App\Link;

$factory->define(App\Visitor::class, function (Faker $faker) {
    return [
        'link_id' => factory(Link::class)->create(),
        'city'    => 'Bhopal',
        'ip'      => request()->ip(),
        'region'  => 'Madhya Pradesh',
        'country' => 'IN',
        'loc'     => '22.9667,76.0667',
        'postal'  => '462024',
    ];
});
