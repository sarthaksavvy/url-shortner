<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Helpers\IPLocation;
use App\Helpers\OpenGraphScrapper;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setup()
    {
        parent::setUp();
        $this->withoutExceptionHandling();
        $this->app->instance(IPLocation::class, MockIPLocation::class);
        $this->app->instance(OpenGraphScrapper::class, MockOpenGraphScrapper::class);
    }

    public function mockIp()
    {
        return '139.130.4.5';
    }
}

class MockOpenGraphScrapper extends OpenGraphScrapper
{
    public function get($url)
    {
        return [
            'hybridGraph'=> [
                'title'       => 'Youtube',
                'description' => 'Best website',
                'image'       => 'https://www.youtube.com/yts/img/yt_1200-vfl4C3T0K.png',
            ],
        ];
    }
}
class MockIPLocation extends IPlocation
{
    public function get()
    {
        return $this;
    }

    public function ip()
    {
        return '111.222.333.444';
    }

    public function city()
    {
        return 'Bhopal';
    }

    public function region()
    {
        return 'Madhya Pradesh';
    }

    public function country()
    {
        return 'IN';
    }

    public function loc()
    {
        return '22.3454,77.44454';
    }

    public function postal()
    {
        return '462024';
    }
}
