<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Visitor;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VisitorModelTest extends TestCase
{
    use DatabaseMigrations;

    /** @test
     * Test for relationship
     */
    public function it_belongs_to_a_link()
    {
        $visitor = factory(Visitor::class)->create();
        $this->assertEquals(1, $visitor->link->count());
    }
}
