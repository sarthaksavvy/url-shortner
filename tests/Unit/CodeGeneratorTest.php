<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Helpers\CodeGenerator;

class CodeGeneratorTest extends TestCase
{
    /** @test
     * Test for generating unique code
     */
    public function it_can_return_a_unique_code_for_a_given_url()
    {
        $code = new CodeGenerator;
        $code = $code->get();
        $this->assertNotNull($code);
        $this->assertEquals(6, strlen($code));
    }
}
