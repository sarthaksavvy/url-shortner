<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Helpers\IPLocation;

class IPLocationTest extends TestCase
{
    /** @test
     * Test for getting locatin from ip address
     */
    public function it_can_get_location_from_ip_address()
    {
        $ip = $this->mockIp();
        $location = new IPLocation;
        $location->ip = $ip;
        $this->assertEquals($ip, $location->get()->ip);
    }
}
