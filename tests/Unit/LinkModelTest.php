<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Link;
use App\Visitor;

class LinkModelTest extends TestCase
{
    use DatabaseMigrations;

    /** @test
     * Test for saving short code to database
     */
    public function it_can_save_data_with_short_code()
    {
        $link = factory(Link::class)->make();
        Link::create([
            'original_url' => $link->original_url,
            'short_code'   => $link->short_code,
        ]);
        $this->assertDatabaseHas('links', ['short_code'=>$link->short_code]);
    }

    /** @test */
    public function it_can_have_many_visitors()
    {
        $link = factory(Link::class)->create();
        $visitor = factory(Visitor::class)->create(['link_id'=>$link->id]);
        $this->assertEquals(1, $link->visitor->count());
    }
}
