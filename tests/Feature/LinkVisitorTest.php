<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Link;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LinkVisitorTest extends TestCase
{
    use DatabaseMigrations;

    /** @test
     * Test for visitors page route
     */
    public function user_can_visit_link_visitor_page()
    {
        $link = factory(Link::class)->create();
        $this->get(route('redirect', $link->short_code));
        $this->get(route('link.visitors', $link->short_code))
            ->assertOk()
            ->assertSee($link->visitor->first()->ip);
    }
}
