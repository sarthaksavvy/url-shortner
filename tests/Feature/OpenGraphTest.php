<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Link;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class openGraphTest extends TestCase
{
    use DatabaseMigrations;

    /** @test
     * Test for open graph data in database
     */
    public function while_visiting_a_page_open_graph_data_is_saved_for_original_url()
    {
        $link = factory(Link::class)->create(['original_url' => 'https://bitfumes.com']);
        $this->get(route('redirect', $link->short_code));
        $this->assertDatabaseHas('open_graphs', ['link_id' => $link->id]);
    }

    /** @test
     * Test for og data only save once
     */
    public function opengraph_data_can_only_be_saved_once()
    {
        $link = factory(Link::class)->create(['original_url' => 'https://bitfumes.com']);
        $this->get(route('redirect', $link->short_code));
        $this->assertDatabaseHas('open_graphs', ['link_id' => $link->id, 'id'=>1]);
        $this->get(route('redirect', $link->short_code));
        $this->assertDatabaseMissing('open_graphs', ['link_id' => $link->id, 'id'=>2]);
    }

    /** @test
     * Test for visit og route
     */
    public function a_user_can_visit_og_detail_page()
    {
        $link = factory(Link::class)->create(['original_url' => 'https://bitfumes.com']);
        $this->get(route('redirect',$link->short_code));
        $this->get(route('link.og', $link->short_code))->assertOk();
    }
}
