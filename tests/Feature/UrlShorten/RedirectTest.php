<?php

namespace Tests\UrlShorten\Feature;

use Tests\TestCase;
use App\Link;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RedirectTest extends TestCase
{
    use DatabaseMigrations;

    /** @test
     * Test for redirect
     */
    public function when_user_go_to_short_url_he_is_redirected_to_original_url()
    {
        $link = factory(Link::class)->create();
        $this->get(route('redirect', $link->short_code))->assertRedirect($link->original_url);
    }
}
