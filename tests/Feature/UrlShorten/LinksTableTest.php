<?php

namespace Tests\UrlShorten\Feature;

use Tests\TestCase;
use App\Link;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LinksTableTest extends TestCase
{
    use DatabaseMigrations;

    /** @test
     * Test for checking route
     */
    public function a_user_can_see_all_short_links_in_table()
    {
        $link = factory(Link::class)->create();
        $this->get(route('url-shortner'))
            ->assertSee($link->short_code);
    }
}
