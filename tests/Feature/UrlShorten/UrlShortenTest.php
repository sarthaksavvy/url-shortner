<?php

namespace Tests\UrlShorten\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Link;

class UrlShortenTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test for checking route
     * @test
     * @return void
     */
    public function a_user_can_visit_url_shortner_page()
    {
        $this->get(route('url-shortner'))
                ->assertOk()
                ->assertSee('Shorten your URL');
    }

    /** @test
     * Test for post url data
     */
    public function original_url_has_to_be_a_valid_url_and_redirect_back_with_error()
    {
        $this->withExceptionHandling(); // to get errors in session

        $this->get(route('url-shortner')); // just for checking redirect url

        $this->post(route('url-shortner'), ['original_url' => 'anInvalidUrl'])
            ->assertRedirect(route('url-shortner'))
            ->assertSessionHasErrors(['original_url']);
    }

    /** @test
     * Test for validation for original url
     */
    public function a_valid_original_url_does_not_give_error()
    {
        $this->post(route('url-shortner'), ['original_url' => 'https://google.com'])
            ->assertSessionHasNoErrors();
    }

    /** @test
     * Test for saving of data in links table
     */
    public function while_creating_short_url_it_save_data_also()
    {
        $url = 'https://google.com';
        $this->post(route('url-shortner'), ['original_url' => $url]);
        $this->assertDatabaseHas('links', ['original_url'=>$url]);
    }

    /** @test
     * Test for flash message
     */
    public function while_creating_short_url_it_return_back_with_flash_message()
    {
        $link = factory(Link::class)->make();

        $this->get(route('url-shortner'));
        $this->post(route('url-shortner'), ['original_url' => $link->original_url])
            ->assertRedirect(route('url-shortner'))
            ->assertSessionHas(['message']);
        $this->get(route('url-shortner'))->assertSee('Short Url is created successfully.');
    }
}
