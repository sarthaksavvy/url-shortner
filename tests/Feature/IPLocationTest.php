<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Link;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class IPLocationTest extends TestCase
{
    use DatabaseMigrations;

    /** @test
     * Test for redirect and store location
     */
    public function when_user_go_to_short_url_his_location_is_catched_via_ip()
    {
        $link = factory(Link::class)->create();
        $this->get(route('redirect', $link->short_code), ['REMOTE_ADDR' => $this->mockIp()]);
        $this->assertDatabaseHas('visitors', ['link_id'=>$link->id]);
    }
}
