<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('url-shortner', 'LinksController@index')->name('url-shortner');
Route::post('url-shortner', 'LinksController@urlToShortCode');

Route::get('url-shortner/{link}/visitors', 'VisitorController@visitors')->name('link.visitors');

Route::get('/r/{link}', 'RedirectController@redirect')->name('redirect');
Route::get('url-shortner/{link}/opengraph', 'RedirectController@opengraph')->name('link.og');

Route::get('check', function () {
    $siteUrl = 'https://www.microsoft.com/en-in/';
    $requestUrl = 'https://opengraph.io/api/1.1/site/'.urlencode($siteUrl);

    // Make sure you include your free app_id here!  No CC required
    $requestUrl = $requestUrl.'?app_id=XXXXXXXXXXXXXXXXXXXXXXXX';

    $siteInformationJSON = file_get_contents($requestUrl);
    $siteInformation = json_decode($siteInformationJSON, true);
    return $siteInformation;
});
