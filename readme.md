# Laravel Url-Shortner

1. Get short code for any big url
1. It store IP address and location details of user who uses short link
1. Store OpenGraph (og) details of original Url

 Check this link for demo [Url Shortner](http://url-shortner.bitfumes.com)
