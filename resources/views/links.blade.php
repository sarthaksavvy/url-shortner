<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-md-12 mt-5">
                <div class="card">
                    <div class="card-header">Shorten your URL</div>
                    @include('includes.flash')
                    <div class="card-body">
                        <form action="{{ route('url-shortner') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="url">Your URL</label>
                                <input type="url" class="form-control" name="original_url" id="url" placeholder="Type your url to shorten it">
                            </div>

                            <button type="submit" class="btn btn-success btn-sm float-right">Shorten It</button>
                        </form>
                    </div>
                </div>

                {{-- For showing all shorten urls . --}} {{-- When we have authentication system, then we show only logged in user urls --}}
                <div class="card mt-5">
                    <div class="table-responsive-sm">
                    <table class="table">
                        <thead class="thead-dark">
                            <th>Original URL</th>
                            <th>Created At</th>
                            <th>Short URL</th>
                            <th>All Clicks</th>
                            <th>OpenGraph Details</th>
                        </thead>
                        <tbody>
                            @foreach ($links as $link)
                            <tr>
                                <td>
                                    <a href="{{ $link->original_url }}">{{ $link->original_url }}</a>
                                </td>
                                <td>{{ $link->created_at->diffForHumans() }}</td>
                                <td>
                                    <a target="_blank" href="{{ $link->short_url() }}">{{ $link->short_url() }}</a>
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('link.visitors',$link->short_code) }}">
                                        <span class="badge badge-primary">{{ $link->visitor->count() }}</span>
                                    </a>
                                </td>
                                
                                <td class="text-center">
                                    <a href="{{ route('link.og',$link->short_code) }}">
                                        @if($link->OpenGraph)
                                            <span class="badge badge-primary">Go</span>
                                        @else
                                        No Data Yet
                                        @endif
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
