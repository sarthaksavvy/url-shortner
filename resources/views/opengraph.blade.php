<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>OpenGraph Details</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1 mt-5">
                <a href="{{ route('url-shortner') }}" class="btn btn-primary btn-sm">Back</a>
                <div class="card mt-5">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <th>Original URL</th>
                            <th>Short URL</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Image Url</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a href="{{ $link->original_url }}">{{ $link->original_url }}</a>
                                </td>
                                <td>
                                    <a target="_blank" href="{{ $link->short_url() }}">{{ $link->short_url() }}</a>
                                </td>
                                <td class="text-center">{{ $link->OpenGraph->title }}</td>
                                <td class="text-center">{{ $link->OpenGraph->description }}</td>
                                <td class="text-center">
                                    <img src="{{ $link->OpenGraph->image }}" alt="og:image" width="100" height="100">
                                    {{-- <a href="{{ $link->OpenGraph->image }}" target="_blank">
                                        {{ $link->OpenGraph->image }}
                                    </a> --}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>