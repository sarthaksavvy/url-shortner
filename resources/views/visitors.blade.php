<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1 mt-5">
                <a href="{{ route('url-shortner') }}" class="btn btn-primary btn-sm">Back</a>
                <div class="card mt-5">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <th>Original URL</th>
                            <th>Short URL</th>
                            <th>Visitor IP</th>
                            <th>City</th>
                            <th>Region</th>
                            <th>Country</th>
                            <th>Location</th>
                        </thead>
                        <tbody>
                            @foreach ($link->visitor as $visitor)
                            <tr>
                                <td>
                                    <a href="{{ $link->original_url }}">{{ $link->original_url }}</a>
                                </td>
                                <td>
                                    <a target="_blank" href="{{ $link->short_url() }}">{{ $link->short_url() }}</a>
                                </td>
                                <td class="text-center">{{ $visitor->ip }}</td>
                                <td class="text-center">{{ $visitor->city }}</td>
                                <td class="text-center">{{ $visitor->region }}</td>
                                <td class="text-center">{{ $visitor->country }}</td>
                                <td class="text-center">{{ $visitor->loc }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>