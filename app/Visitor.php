<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\IPLocation;

class Visitor extends Model
{
    protected $fillable = ['link_id', 'ip', 'city', 'region', 'country', 'loc', 'postal'];

    /**
     * @param $link_id
     *  Save location details
     */
    public static function createWithIP($link_id)
    {
        $visitor = self::where(['ip'=> request()->ip(), 'link_id'=>$link_id])->exists();
        if (! $visitor) {
            self::create((new self)->data($link_id));
        }
    }

    /**
     * @param $link_id
     * @return array
     *  data for location in a proper array
     */
    public function data($link_id)
    {
        $LocationApi = app(IPLocation::class);
        $location = (new $LocationApi)->get();
        return [
            'link_id' => $link_id,
            'ip'      => request()->ip(),
            'city'    => $location->city(),
            'region'  => $location->region(),
            'country' => $location->country(),
            'loc'     => $location->location(),
            'postal'  => $location->postal(),
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function link()
    {
        return $this->belongsTo(Link::class);
    }
}
