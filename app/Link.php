<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = ['original_url', 'short_code'];

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function short_url()
    {
        return url("r/{$this->short_code}");
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'short_code';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visitor()
    {
        return $this->hasMany(Visitor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function OpenGraph()
    {
        return $this->hasOne(OpenGraph::class);
    }
}
