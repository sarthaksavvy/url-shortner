<?php

namespace App\Helpers;

class OpenGraphScrapper
{
    /**
     * @param $url
     * @return array
     *  return open graph og tag data
     */
    // public function get($url)
    // {
    //     $site_html = file_get_contents($url);
    //     $matches = null;
    //     preg_match_all('~<\s*meta\s+property="(og:[^"]+)"\s+content="([^"]*)~i', $site_html, $matches);
    //     dd($matches);
    //     $ogtags = [];
    //     for ($i = 0; $i < count($matches[1]); $i++) {
    //         $key = str_replace('og:', '', $matches[1][$i]);
    //         $ogtags[$key] = str_replace('og:', '', $matches[2][$i]);
    //     }
    //     return $ogtags;
    // }

    public function get($url)
    {
        $requestUrl = 'https://opengraph.io/api/1.1/site/'.urlencode($url);
        $requestUrl = $requestUrl.'?app_id=XXXXXXXXXXXXXXXXXXXXXXXX';
        // $requestUrl = $requestUrl.'?app_id='.env('OPENGRAPHIO_KEY');
        $siteInformationJSON = file_get_contents($requestUrl);
        return json_decode($siteInformationJSON, true);
    }
}
