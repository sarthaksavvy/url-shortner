<?php

namespace App\Helpers;

use vipnytt\RobotsTxtParser\UriClient;

class OpenGraphHelper
{
    protected $fillable = ['title', 'link_id', 'description', 'image_url'];

    public $link;

    /**
     * OpenGraphHelper constructor.
     * @param $link
     */
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * @return string|void
     *  Check for robots.txt and then save
     */
    public function fetchAndSave()
    {
        $allow = $this->allowed();
        if ($allow) {
            return $this->fetch();
        }
        return 'Not Allow';
    }

    /**
     * @return bool
     * check for robots.txt
     */
    public function allowed()
    {
        $client = new UriClient($this->baseUrl());
        return $client->userAgent('GoogleBot')->isAllowed($this->link->original_url);
    }

    /**
     * @return string
     * return base url
     */
    protected function baseUrl()
    {
        $parse = parse_url($this->link->original_url);
        return "{$parse['scheme']}://{$parse['host']}";
    }

    /**
     * fetch the open graph data
     */
    public function fetch()
    {
        $ogScrapper = app(OpenGraphScrapper::class);
        $siteInformation = (new $ogScrapper)->get($this->link->original_url);
        if (isset($siteInformation['error'])) {
            throw new \Exception('Please wait !!. '.$siteInformation['error']['message'], 1);
        }
        $result = $this->save($siteInformation['hybridGraph']);
        return $result;
    }

    /**
     * @param $details
     *  save og details to db
     */
    protected function save($details)
    {
        if ($details) {
            if (! $this->link->OpenGraph) {
                $this->link->OpenGraph()->create($details);
            }
        }
    }
}
