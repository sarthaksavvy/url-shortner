<?php

namespace App\Helpers;

class IPLocation
{
    public $ip;

    public $result;

    private $path = 'https://ipinfo.io';

    /**
     * IPLocation constructor.
     */
    public function __construct()
    {
        $this->ip = request()->ip();
    }

    /**
     * @return $this
     */
    public function get()
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL            => $this->url(),
        ]);
        $result = curl_exec($curl);
        curl_close($curl);
        $this->result = json_decode($result, true);
        return $this;
    }

    /**
     * @return string
     */
    protected function url()
    {
        return "{$this->path}/{$this->ip}/geo";
    }

    /**
     * @return city
     */
    public function city()
    {
        return $this->result['city'] ?? null;
    }

    /**
     * @return location
     */
    public function location()
    {
        return $this->result['loc'] ?? null;
    }

    /**
     * @return region
     */
    public function region()
    {
        return $this->result['region'] ?? null;
    }

    /**
     * @return country
     */
    public function country()
    {
        return $this->result['country'] ?? null;
    }

    /**
     * @return postal
     */
    public function postal()
    {
        return $this->result['postal'] ?? null;
    }
}
