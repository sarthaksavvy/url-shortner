<?php

namespace App\Helpers;

class CodeGenerator
{
    public $charset = '123456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ';

    /**
     * @return bool|string
     */
    public function get()
    {
        return $this->make();
    }

    /**
     * @return bool|string
     *  return real code
     */
    protected function make()
    {
        $n = rand(6, $this->finalCharsetLen());
        $shuffled = str_shuffle($this->finalCharset());

        return substr($shuffled, $n, 6);
    }

    /**
     * @return string
     * just for making code more unique
     */
    protected function finalCharset()
    {
        return $this->charset.$this->charset;
    }

    /**
     * @return int
     * for creating random Statring point for substr
     */
    protected function finalCharsetLen()
    {
        return strlen($this->finalCharset()) - 6;
    }
}
