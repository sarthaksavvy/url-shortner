<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LinkRequest;
use App\Link;
use App\Helpers\CodeGenerator;

class LinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = Link::latest()->get();
        return view('links', compact('links'));
    }

    /**
     * @param LinkRequest $request
     * @param CodeGenerator $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function urlToShortCode(LinkRequest $request, CodeGenerator $code)
    {
        // Save to db
        $link = Link::create([
            'original_url'=> request('original_url'),
            'short_code'  => $code->get(),
        ]);

        // redirect back
        return back()->with(['message'=>'Short Url is created successfully.']);
    }
}
