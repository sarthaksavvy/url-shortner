<?php

namespace App\Http\Controllers;

use App\Link;
use App\Visitor;
use App\Helpers\OpenGraphHelper;

class RedirectController extends Controller
{
    /**
     * @param Link $link
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirect(Link $link)
    {
        Visitor::createWithIP($link->id);
        $this->fetchOpenGraph($link);
        return redirect($link->original_url);
    }

    /**
     * @param $link
     */
    public function fetchOpenGraph($link)
    {
        $graph = new OpenGraphHelper($link);
        $ogDetails = $graph->fetchAndSave();
    }

    /**
     * @param Link $link
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function opengraph(Link $link)
    {
        if (!$link->OpenGraph) {
            return back();
        }
        return view('opengraph', compact('link'));
    }
}
