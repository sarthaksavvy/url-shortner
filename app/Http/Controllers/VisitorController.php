<?php

namespace App\Http\Controllers;

use App\Link;

class VisitorController extends Controller
{
    /**
     * @param Link $link
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function visitors(Link $link)
    {
        return view('visitors', compact('link'));
    }
}
