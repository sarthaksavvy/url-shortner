<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenGraph extends Model
{
    protected $fillable = ['title', 'description', 'link_id', 'image'];

    /**
     * Relationship for link model
     */
    public function Link()
    {
        $this->belongsTo(Link::class);
    }
}
